If you're using Docker from the command line, you can sign in to your Docker Hub account with the following command:

```
$ docker login
```

You have to provide your username and password. If you have 2FA enabled, you must create a token to be used as password for logging.

Authentication details are stored in the `~/.docker/config.json` file.

The file will store the credentials in plain text and it will look like this:

```
{
  "auths": {
    "https://index.docker.io/v1/": {
	  "auth": "dXNlcm5hbWU6cGFzc3dvcmQ="
    }
  }
}
```

The file uses `base64` to encode authentication details. For example, if you decode the value of `auth`, you will get:

```
$ echo -n 'dXNlcm5hbWU6cGFzc3dvcmQ=' | base64 --decode
username:password
```

In that case, a more secure way to manage your credentials will be using a [credential helper](https://github.com/docker/docker-credential-helpers/).

These are the available programs that can be used to configure a credential helper:

* `osxkeychain`: Provides a helper to use the OS X keychain as credentials store.
* `secretservice`: Provides a helper to use the D-Bus secret service as credentials store.
* `wincred`: Provides a helper to use Windows credentials manager as store.
* `pass`: Provides a helper to use pass as credentials store.

On Linux, you can use `secretservice`. To install it run the following commands:

```
wget -O docker-credential-secretservice https://github.com/docker/docker-credential-helpers/releases/download/v0.8.0/docker-credential-secretservice-v0.8.0.linux-amd64
chmod +x docker-credential-secretservice
sudo mv docker-credential-secretservice /usr/local/bin/
```

Then set the `credsStore` option in your `~/.docker/config.json` file:

```
sed -i '0,/{/s/{/{\n\t"credsStore": "secretservice",/' ~/.docker/config.json
```

Logout:

```
docker logout
```

Credentials are removed from the `~/.docker/config.json` file.

And login again:

```
docker login
```

It will ask you for your username and password again, but this time the `~/.docker/config.json` file will have the following content:

```
{
  "auths": {
    "https://index.docker.io/v1/": {}
  },
  "credsStore": "secretservice"
}
```

When running Docker Desktop, a credential helper is provided, and you don't need to configure one manually. The `~/.docker/config.json` file, in Windows, has the following content:

```
{
  "auths": {
	"https://index.docker.io/v1/": {}
  },
  "credsStore": "desktop.exe"
}
```

Now you can manage your credentials in a more secure way.