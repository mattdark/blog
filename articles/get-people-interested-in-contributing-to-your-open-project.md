So you have a project that you decided to publish under an open license, what's next? How do you get people interested in contributing to your open project?

First, when I say open project, I mean, any project released under a license like [GPL](https://www.gnu.org/licenses/licenses.html), any Open Source [license](https://opensource.org/licenses/), or a [Creative Commons](https://creativecommons.org) license. Not every project involves software development. There are projects that are related to the creation of multimedia content, like images, text, audio or video, and if you want that anyone has access, can redistribute the material or create derivative work, as it happens with Free and Open Source Software (FOSS), you'll have to use a Creative Commons license, or GFDL if it's a documentation project.

On my previous blog post, [A Non-Coders Guide to Open Source Contributions](https://dev.to/mattdark/a-non-coders-guide-to-open-source-contributions-13ji), I wrote about how to contribute to Open Source in areas not related to software development, and it's something to be considered when looking for people to join our project as contributors. You may need a logo for your project, someone to write the documentation, or translate it to another language.

Through this blog post, you will learn how to prepare your project and invite people to contribute.

## First Steps
Here are some steps to follow for preparing your project:

* Create a repository for your project
    * Follow [this guide](https://www.atlassian.com/git/tutorials/setting-up-a-repository) by Atlassian

* Document your project and process
    * Create a `README`
        According to Hillary Nyakundi, on [this article](https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/), published at [freeCodeCamp](https://www.freecodecamp.org), a `README` is a file that gives users a detailed description of a project you have worked on. 

        It can also be described as documentation with guidelines on how to use a project. Usually it will have instructions on how to install and run the project.

        You can use a template to create the `README` for your project:
            * [GitHub README Templates](https://www.readme-templates.com/)
            * [A template to make good README.md](https://github.com/PurpleBooth/a-good-readme-template)

        Use the editor at [readme.so](https://readme.so)

        Or follow the recommendations at [Make a README](https://www.makeareadme.com)

    * Add a `CONTRIBUTING.md` file
        This file contains instructions on how people can contribute to your project, even if it's within the code, documentation, translations or other kind of contributions.

        Some useful resources:
            * [How to Build a CONTRIBUTING.md](https://mozillascience.github.io/working-open-workshop/contributing/)
            * [Setting guidelines for repository contributors](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/setting-guidelines-for-repository-contributors)

        Need an example? Check the GitHub docs [contributing guide](https://github.com/github/docs/blob/09be36847cdeaa64867b61c8d0005db005b61843/.github/CONTRIBUTING.md).
        
    * Add the `LICENSE` file
        Depending on the characteristics of your project, as I mentioned previously, you can use a license like [GPL](https://www.gnu.org/licenses/licenses.html) or any Open Source [license]([license](https://opensource.org/licenses/)) approved by the Open Source Initiative, or a [Creative Commons](https://creativecommons.org) license for multimedia content, or GFDL if it's a documentation project.

        [Choose a License](https://choosealicense.com/) may help you choose the right license for your project.

    * Add the `CODE_OF_CONDUCT.md` file
        According to the GitHub Docs, a code of conduct defines standards for how to engage in a community. It signals an inclusive environment that respects all contributions. It also outlines procedures for addressing problems between members of your project's community.

        Many Open Source projects have adopted the [Contributor Covenant](https://www.contributor-covenant.org/) as their code of conduct. Check [this page](https://www.contributor-covenant.org/adopters/) to see a list of adopters.

    * Create the technical documentation of your project
        You can use any of the following options:
            * A wiki, like the [ArchWiki](https://wiki.archlinux.org) that uses [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki)
            * [Read the Docs](https://about.readthedocs.com/), used by projects like [Setuptools](https://setuptools.pypa.io/en/latest/). Check [Awesome Read the Docs](https://github.com/readthedocs-examples/awesome-read-the-docs) for more examples.
            * Create a website
            * Create a blog, like the documentation of [Blowfish](https://blowfish.page/), a theme for [Hugo](https://gohugo.io/). 

* Setup communication channels
    There are many options to set up the communication channels for your project, but use the less possible. For someone without a technical background, it can be difficult to learn how to use as many new tools as you may probably be using. Keep communication as simple as possible.

    Here are some options:
        * Mailing lists
        * Gitter, Slack, IRC, Telegram
        * Social media

## Create Issues
From the GitLab Documentation, use [issues](https://docs.gitlab.com/ee/user/project/issues/) to collaborate on ideas, solve problems, and plan work. Share and discuss proposals with your team and with outside collaborators.

How to add the first issues to your project? First, make a list of tasks that need to be done. You can use:

* A whiteboard/window
* A notebook or post-it notes
* Note-taking apps. Here's a list of [note-taking apps for Linux]((https://itsfoss.com/note-taking-apps-linux/))

Then, add the issues on your project repository. Check the instructions for:

* [GitHub](https://docs.github.com/en/issues/tracking-your-work-with-issues/creating-an-issue)
* [GitLab](https://docs.gitlab.com/ee/user/project/issues/create_issues.html)

Don't forget to label your issues appropiately. For people with no previous experience contributing to Open Source or any open project, you must consider including issues with the following labels:

* `good first issue`
* `help wanted`
* `first timers only`

More information about labels:

* [GitHub](https://docs.github.com/en/issues/using-labels-and-milestones-to-track-work/managing-labels)
* [GitLab](https://docs.gitlab.com/ee/user/project/labels.html)

Issues can be technical or non-technical, and related to:
* Bug report
* Feature request
* Documentation
* Localization
* UI/UX
* Design

### What's in an Issue?
A good issue has:

* Instructions on how to get started
* Clear requirements
* Pointers to relevant information, including:
    * Contributing guidelines
    * Code of conduct
    * License

Your users and contributors may also be able to create issues for reporting bugs or requesting new features. Here you will find more information on how to add templates for your issues, and pull requests or merge requests:

* [GitLab - Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html)
* [GitHub - Configuring issue templates for your repository](https://docs.github.com/en/communities/using-templates-to-encourage-useful-issues-and-pull-requests/configuring-issue-templates-for-your-repository)

When a new contributor arrives:
* Answer with a welcome message like "Thanks, here to help if you have any questions!", or add a [welcome-bot](https://github.com/behaviorbot/welcome)
* Make a commitment to respond to inquiries

### What is a pull/merge request?
A [pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests), as defined by GitHub, is a proposal to merge a set of changes from one branch into another. In a pull request, collaborators can review and discuss the proposed set of changes before they integrate the changes into the main codebase. Pull requests display the differences, or diffs, between the content in the source branch and the content in the target branch.

GitLab defines a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) like a proposal to incorporate changes from a source branch to a target branch. Merge requests include:

* A description of the request.
* Code changes and inline code reviews.
* Information about CI/CD pipelines.
* A comment section for discussion threads.
* The list of commits. 

## Mentoring Contributors
`first timers only` issues are good for onboarding new contributors through mentorship. Many people start contributing to Open Source expecting to learn new technologies or improve their skills. If you have the time for guiding contributors, add your name as a mentor in the issue.

Should continue mentoring this contributor? Here are some general guidelines when choosing which contributors to invest in:

* **Available**. Has the time and wants to contribute and learn
* **Teachable**. Able to learn from you and the community
* **Passionate**. They have a genuine interest in your project and mission
* **Skills**. Do they have the skills you need on your project?

### Ongoing support

* Stay tuned to the messages sent to the communication channels set up for your project, and answer any newcomer questions that might come up
* Help newcomers to Git as they go through their first issue
    Here are some good resources you can share:
        * [Getting started with Git](https://docs.github.com/en/get-started/getting-started-with-git)
        * [Learn Git](https://docs.gitlab.com/ee/topics/git/)
* Point people to good first issues and follow up with them

## After Contribution
Once they contribute/submit a pull/merge request:
* Thank them for their work. You can use the [welcome-bot]
* Give good, consistent and helpful feedback during a review - ask questions about what they’ve done

After their work is merged in:
* Appreciate their work. You can add an [all-contributors](https://github.com/kentcdodds/all-contributors) section to the README of your project
* Point them to a good next issue / next task for them to look at

Have some swag to give away? Stickers, t-shirts, etc. That's another way to thank your contributors for their work and the time they dedicate to the project.

## Attracting contributors
Want to spread the word about your Open Source project? Depending on your availability, here are some options to take into account that may help attract contributors to your project:

* Create a mailing list to keep your users and contributors updated about your project
* If you are already registered to any mailing list, send an email with information about your project and contributing opportunities
* Post information about your project on social media
* Use messaging apps to invite people
* Write a blog post about the project
* Create a video and post it on social media
* Attend and/or organize events
    * Organize a workshop
    * Organize hackathon
    * Join [Hacktoberfest](https://hacktoberfest.com/)
    * Speak at a conference/meetup

### The Pac-Man Rule
Attending events can be an opportunity to spread the word about your project, and you may find some people who would be interested in contributing to your project. 

While attending an event and joining conversations, don't forget to follow the [Pac-Man rule](https://www.ericholscher.com/blog/2017/aug/2/pacman-rule-conferences/), invented by [Eric Holscher](https://www.ericholscher.com/), that is:

    When standing as a group of people, always leave room for 1 persona to join your group

Like a Pac-Man.

![Pac-Man](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Pacman.svg/456px-Pacman.svg.png)

Leaving room for new people when standing in a group is a physical way to show an inclusive and welcoming environment.

## Conclusion
Through this blog post you learned some ways you can spread the word about your project and get people interested in contributing to your open project.