What's the first thing that comes to mind when you hear "Open Source Contributions"? You're probably thinking that you have to learn any programming language, or you have to study a career in technology, or you need a technical background, but the truth is that anyone can contribute.

My first approach to Open Source was back in 2006, when I installed Ubuntu, the first Linux distribution I used, and my first contributions didn't happen until a few years later. 

In 2008, I was invited to be a speaker at a local event, where I presented a talk for talking about my experience using Linux, and I've been attending events since then for spreading the word about the different projects I've joined and tools and technologies I've used.

Speaking at conferences has been a way to contribute to Open Source, but I've also made contributions to other areas like localization, technical writing, software development and event organization.

While I'm a software developer and studied a career in technology, it doesn't mean that those are requirements for contributing or building a career in Open Source.

Through this blog post, you'll learn how to contribute to Open Source without having a technical background.

## How To Contribute
If you found a project you love and want to help, bear in mind that:

* Coding skills are not required
* You don't have to install another operating system or get familiar with the command line
* You don't have to learn another language
* You don't have to be an expert, but Be excited about learning and sharing

And some of the areas where you can contribute other than software development are:
* Design
* Documentation
* Localization/Translation
* Support
* Mentorship
* Evangelism

## Design
Through the years I've learned how to use some Open Source design tools like [Inkscape](https://inkscape.org/), [GIMP](https://www.gimp.org/) and [Krita](https://krita.org/en/). While I'm not an expert on this area, I've used these tools to create graphics for some of my personal projects, and recently the logo of [Let's Talk! Open Source](https://instagram.com/letstalkoss), that I created using Inkscape.

You can find some ideas of [how to contribute](https://opensource.guide/how-to-contribute/) to Open Source as a designer on the [Open Source Guides](https://opensource.guide) by [GitHub](https://github.com).

* Restructure layouts to improve the project’s usability
* Conduct user research to reorganize and refine the project’s navigation or menus, [like Drupal suggests](https://www.drupal.org/community-initiatives/drupal-core/usability)
* Put together a style guide to help the project have a consistent visual design
* Create art for t-shirts or a new logo, [like hapi.js’s contributors did](https://github.com/hapijs/contrib/issues/68)

But, do designers belong in Open Source? I will let Veethika Mishra, Senior Product Designer at [GitLab](https://about.gitlab.com/), to answer this question through [this article](https://blog.prototypr.io/do-designers-belong-in-open-source-160bb97e3b78).

## Documentation
You can contribute to the documentation of a project by improving the content. When exploring the documentation, you may find typos, misinformation, wrong instructions or outdated information. You can report those pages that need improvement by contacting the project maintainers or making those changes by yourself, creating a [pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests) or [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) in the Git repository.

If you're new to Git, you can follow these guides:

* [Creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
* [Creating a pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/creating-a-pull-request)

Here are some examples of pull requests I created in the documentation of Percona, while working there:

{% embed https://github.com/percona/pbm-docs/pull/82 %}

{% embed https://github.com/percona/psmdb-docs/pull/648 %}

{% embed https://github.com/percona/pdmysql-docs/pull/51 %}

Not every answer for each question you may have about a technology or tool you're using can be found in the official documentation. If you solved a problem that wasn't well documented, and you like to write, creating articles and tutorials is another way to contribute to an Open Source project.

## Localization/Translation
According to [this article](https://phrase.com/blog/posts/software-localization/#what-is-software-localization) from [Phrase](https://phrase.com), software localization is a process in software development that aims to adapt a web or mobile app to the culture and language of users in a target market.

Localization goes beyond mere translation. While translation is focused on transferring words between languages, localization involves adapting images, colors, date and time formats, UI elements, and more to make a software product look and feel as native as possible in a target market.

As I've told you before, in other blog posts, English is my second language and started learning 20 years ago, when I was in high school. I was given the opportunity to take English lessons in a local university for about 4 and a half years.

When I decided to pursue a career in technology, I already spoke English, which put me in a privilege position. In Latin America, not everyone has access to study English as a second language.

When I joined the Mozilla community back in 2011, I already was a software developer, but prefered to contribute to the localization of Firefox, as I thought it was more valuable to help localize the browser into my native language, Spanish.

Mozilla uses [Pontoon](https://github.com/mozilla/pontoon), a translation management system used and developed by the [Mozilla localization community](https://pontoon.mozilla.org/). It specializes in open source localization that is driven by the community and uses version-control systems for storing translations.

You can take a look at the different localization projects [here](https://pontoon.mozilla.org). 

When talking about documentation, most of the content about Open Source, software development or any technology related topic is available in English and while there are efforts to localize tools and translate documentation into other languages, it's important that we, as project maintainers, content creators or speakers, make sure the documentation or any content created is also available in our native language.

Updated information about any technology or tool is available in English, and translations to other languages could take years, especially with books. In that case, if you speak English as your second language, you may want to help translate the official documentation of your favorite project into your native language, as the Python community is doing with the translation of the documentation into different languages. You can find more information about this project at the [Python Developer's Guide](https://devguide.python.org/documentation/translating/).

I started writing on DEV back in 2019, with most of the content available in English, but since 2023, every blog post published here is also available in Spanish on [my website](https://mariog.dev).

## Support
If you have a small amount of time, you can support others by:

* Answering questions on:
    * Stack Overflow
    * Reddit
    * Forums
    * Blogs
    * Social media
* Answering questions on a 1:1 conversation
* Helping solve issues
* Recommend Open Source alternatives

You don't need to be an expert to provide support, you can share what you've learned after using Open Source tools, or contributing to any project.

## Mentorship
From my blog post about [My Rules for Being a Tech Speaker](https://dev.to/mattdark/my-rules-for-being-a-tech-speaker-4g4o), as a mentor to some Open Source projects and individuals, I've identified these key reasons of why to provide mentorship:

* Promote personal and professional development. Mentorships can be an opportunity for a mentor to identify:
 * What technologies (the mentor) is passionate about
 * Area of expertise
 * Skills that need to be improved or acquired

* Improve your communication skills. Preparing a mentorship session implies not only having knowledge about the topic. In technology, it can be difficult to explain technical concepts in a clear and concise way, without it being more complex to understand for less experienced people or a non-technical background.

* Improve your leadership skills. A mentor is a guide for people to succeed towards their goals, no matter what they want to achieve, like becoming a better developer or being successful in their first speaking engagement. To your mentees, you will: 
 * Be a source of inspiration
 * Show them the way forward
 * Provide support and tools needed to achieve their goals

    And that's what a [good leader](https://www.betterup.com/blog/what-is-a-leader-and-how-do-you-become-one) do.

* Expand connections and networks. Providing mentorship can be an opportunity to meet people from all over the world and know different cultures, as well as a room for future collaborations.

* Mutual learning. Mentors are knowledgeable people in their area of expertise, but they don't know everything. In a mentoring relationship, both sides must be open to learn from each other. New technologies can be discovered in a mentorship session, and those can be useful for future projects.

## Evangelism
Want to spread the word about your favorite Open Source project? Depending on your availability, here are some options to take into account:

* Organize events
    * Meetups
    * Conferences
    * Workshops
    * Hackathons
    * Webinars
* Be a speaker
* Share the project
    * Mailing lists
    * Social networks
    * Messaging apps

The idea behind organizing events is that more people know about a specific project or Open Source in general, as well as how to contribute, no matter the background or experience attendees could have. It doesn't have to be a big event, small events are a good starting points. Organizing an event requires time, and more people being involved is better, as you don't have to take care of everything.

The format of the event will depend on how many people you're expecting, the venue size, date and time, and the time you're committed to dedicate. Most Open Source events are organized by volunteers. You can also join the team of an event already happening in your city.

Webinars are also a good option to consider. The advantage of virtual events is that more people can get access.

Would you like to improve your public speaking skills? Consider being a speaker and participate at local events and share your experience with Open Source. Read the article I wrote about [My Rules for Being a Tech Speaker](https://dev.to/mattdark/my-rules-for-being-a-tech-speaker-4g4o/).

## Conclusion
Based on my prior experience, I have recommended certain areas where you can contribute without possessing a technical background. Furthermore, contributing to Open Source is an opportunity you may consider as you can learn, acquire new skills, meet people, and gain experience that help you build your career.