Through the years, I've used different virtualization tools, mainly Open Source, as a way to test other Linux distributions, configure development environments, or to teach some concepts on various topics.

Some of these tools include [Oracle VM VirtualBox](https://virtualbox.org) (that I've used since before the acquisition of Sun Microsystems by Oracle), [VMWare Workstation Player](https://www.vmware.com/content/vmware/vmware-published-sites/us/products/workstation-player.html.html),  and [QEMU](https://www.qemu.org/), but last year, I found out about Multipass.

![The Fifth Element - Multipass Scene](https://media.giphy.com/media/uIGHPjEfdc0Ni/giphy.gif)

No, not the one from that scene of [The Fifth Element](https://en.wikipedia.org/wiki/The_Fifth_Element) with [Milla Jovovich](https://en.wikipedia.org/wiki/Milla_Jovovich) as Leeloo.

## What Is Multipass?

[Multipass](https://multipass.run/docs) is a tool to generate cloud-style Ubuntu VMs quickly on Linux, macOS, and Windows. It gives you a simple but powerful CLI that allows you to quickly access an Ubuntu command line or create your own local mini-cloud.

It uses KVM on Linux, Hyper-V on Windows and QEMU on macOS to run the VM with minimal overhead. It can also use VirtualBox on Windows and macOS. Multipass will fetch images for you and keep them up to date.

Developed by [Canonical](https://canonical.com).

## Installation
Multipass is available as a [Snap](https://snapcraft.io/about) package on Linux. The [snap daemon](https://snapcraft.io/docs/installing-snapd) (`snapd`) is pre-installed on the following:

- [KDE Neon](https://neon.kde.org/)
- [Manjaro](https://manjaro.org/)
- [Solus](https://getsol.us/)
- [Ubuntu](https://ubuntu.com/) 18.04 and above
- Most [Ubuntu flavors](https://wiki.ubuntu.com/DerivativeTeam/Derivatives#Official_Ubuntu_Flavors)
- [Zorin OS](https://zorinos.com/)

Otherwise, follow the instructions for your distributions in the [docs](https://snapcraft.io/docs/installing-snapd).

Once `snapd` is available on your system, you can install Multipass by running the following command:

```bash
$ snap install multipass
```

For architectures other than `amd64`, you’ll need the `beta` channel at the moment.

You can also use the `edge` channel to get the latest development build:

```bash
$ snap install multipass --edge
```

If you want to uninstall Multipass, just run:

```bash
$ snap remove multipass
```

To install on macOS, follow the instructions in the [docs](https://multipass.run/docs/installing-on-macos).

The docs also have instructions for installing Multipass on [Windows](https://multipass.run/docs/installing-on-windows).

## Create an Instance

To create an instance, just run:

```bash
$ multipass launch
```

The above command will create a instance using an image of the latest LTS version of Ubuntu (`22.04.3`). Multipass will assign a random name to the instance and it will apply default configuration.

You can list the instances on your system by running:

```bash
$ multipass list
Name                    State             IPv4             Image
visionary-longspur      Running           10.81.157.45     Ubuntu 22.04 LTS
```

You can check the details of your instance with the following command:

```bash
$ multipass info visionary-longspur
Name:           visionary-longspur
State:          Running
IPv4:           10.81.157.45
Release:        Ubuntu 22.04.3 LTS
Image hash:     054db2d88c45 (Ubuntu 22.04 LTS)
CPU(s):         1
Load:           2.30 0.82 0.29
Disk usage:     1.4GiB out of 4.8GiB
Memory usage:   157.2MiB out of 951.9MiB
Mounts:         --
```

By default, Multipass will assign the following resources:

- 1 CPU
- 5GB of disk
- 1GB of RAM

Default user is `ubuntu`.

## Create a Customized Instance

You can create a customized instance. First, check what images are available:

```bash
$ multipass find
Image                       Aliases           Version          Description
core                        core16            20200818         Ubuntu Core 16
core18                                        20211124         Ubuntu Core 18
core20                                        20230119         Ubuntu Core 20
core22                                        20230717         Ubuntu Core 22
20.04                       focal             20231011         Ubuntu 20.04 LTS
22.04                       jammy,lts         20231026         Ubuntu 22.04 LTS
23.04                       lunar             20231025         Ubuntu 23.04
23.10                       mantic,devel      20231011         Ubuntu 23.10
appliance:adguard-home                        20200812         Ubuntu AdGuard Home Appliance
appliance:mosquitto                           20200812         Ubuntu Mosquitto Appliance
appliance:nextcloud                           20200812         Ubuntu Nextcloud Appliance
appliance:openhab                             20200812         Ubuntu openHAB Home Appliance
appliance:plexmediaserver                     20200812         Ubuntu Plex Media Server Appliance

Blueprint                   Aliases           Version          Description
anbox-cloud-appliance                         latest           Anbox Cloud Appliance
charm-dev                                     latest           A development and testing environment for charmers
docker                                        0.4              A Docker environment with Portainer and related tools
jellyfin                                      latest           Jellyfin is a Free Software Media System that puts you in control of managing and streaming your media.
minikube                                      latest           minikube is local Kubernetes
ros-noetic                                    0.1              A development and testing environment for ROS Noetic.
ros2-humble                                   0.1              A development and testing environment for ROS 2 Humble.
```

Create an instance with the following configuration:

- Ubuntu Mantic Minotaur (`23.10`)
- Custom name: `server`
- 2 CPUs
- 10GB of disk
- 2 GB of RAM

```bash
$ multipass launch mantic --name server --cpus 2 --disk 10G --memory 2G
```

Where `mantic` is the name of the image being used for creating the instance.

## Basic Commands
### Open a Shell Prompt Inside an Instance
To open a shell prompt on an existing instance `server`, execute the following command:

```bash
$ multipass shell server
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 5.15.0-87-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Fri Nov 10 01:32:53 CST 2023

  System load:  0.0               Processes:             90
  Usage of /:   30.9% of 4.67GB   Users logged in:       0
  Memory usage: 19%               IPv4 address for ens3: 10.81.157.45
  Swap usage:   0%


Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
```

The instance will be started automatically if it is stopped or suspended.

You can run commands as you would do on any Ubuntu installation.

### Execute a Command Inside an Instance
Commands can be executed inside an instance directly from the host, by running

```bash
$ multipass exec server -- pwd
/home/ubuntu
```

### Transfer a File From the Host to the Instance
If you want to transfer a file, like a Bash script, from the host to the instance, run the following command:

```bash
$ multipass transfer install.sh server:/home/ubuntu/install.sh
```

`install.sh` is the file from the host that will be transfered to the instance.

`server` is the name of the instance.

`/home/ubuntu/install.sh` is the path inside the instance where the file will be stored.

### Change Status of the Instance
#### Stop an Instance
To stop an instance, run:

```bash
$ multipass stop server
```

You can stop as many instances as needed with a single command:

```bash
$ multipass stop server server2 server3
```

All running instances can be stopped at once:

```bash
$ multipass stop --all
```

#### Start an instance
To start an instance, run:

```bash
$ multipass start server
```

You can start as many instances as needed with a single command:

```bash
$ multipass start server server2 server3
```

All stopped or suspended instances can be started at once:

```bash
$ multipass stop --all
```

#### Suspend an instance
To suspend an instance, run:

```bash
$ multipass suspend server
```

You can suspend as many instances as needed with a single command:

```bash
$ multipass suspend server server2 server3
```

All running instances can be suspended at once:

```bash
$ multipass suspend --all
```

### Remove an instance
You can delete an instance by running the following command:

```bash
$ multipass delete server
```

Or as many instances as needed:

```bash
$ multipass delete server server2 server3
```

Running instances will be stopped and deleted.

After deleting an instance, run:

```bash
$ multipass purge
```

The multipass purge command will permanently remove all instances deleted with the multipass delete command. This will destroy all the traces of the instance, and cannot be undone.

You're ready to go. Ubuntu VMs can be created at ease using Multipass.