This blog post is the second part of the series on Percona Backup for MongoDB (PBM) that includes the following articles:

1. Deploy a MongoDB Cluster With Docker
2. Object Storage with MinIO
3. Running Percona Backup for MongoDB With Docker

Through this blog post, you will learn how to configure MinIO to use it with PBM for storing the backup of your databases. As in the previous blog post, we will use Docker for configuring MinIO in a development environment.

Percona Backup for MongoDB supports the following types of [remote backup storage](https://docs.percona.com/percona-backup-mongodb/details/storage-configuration.html):

* S3-compatible storage
* Filesystem type storage
* Microsoft Azure Blob storage

It is proven to work with the following S3-compatible storages:

* [Amazon Simple Storage Service](https://docs.aws.amazon.com/s3/index.html)
* [Google Cloud Storage](https://cloud.google.com/storage)
* [MinIO](https://min.io/)

## Object Storage with MinIO
[MinIO](https://min.io/docs) is an object storage solution that provides an Amazon Web Services S3-compatible API and supports all core S3 features. MinIO is built to deploy anywhere - public or private cloud, baremetal infrastructure, orchestrated environments, and edge infrastructure.

You can install and configure MinIO using Docker and Kubernetes, as well as install it on your operating system, with support for Linux, Mac, and Windows.

You can follow the instructions in [the documentation](https://min.io/docs/minio/container/index.html) to run it using Docker.

First, you need to create a local directory:

```
$ mkdir -p ~/minio/data
```

This directory will serve as a persistent volume location for the container to use.

Then, start the container by running the following command:

```
docker run \
   -p 9000:9000 \
   -p 9001:9001 \
   --name minio \
   --network mongodbCluster \
   -v ~/minio/data:/data \
   -e "MINIO_ROOT_USER=username" \
   -e "MINIO_ROOT_PASSWORD=password" \
   quay.io/minio/minio server /data --console-address ":9001"
```

The parameters in the above command are:

`-p`. The ports in the host machine that will receive and redirect all the requests to the ports in the container
`--name`. Name of the container
`--network`. Name of the network to use
`--v`. Directory used as a persistent volume for the container
`-e`. Sets the `MINIO_ROOT_USER` and `MINIO_ROOT_PASSWORD` environment variables

The container is built from the repository of MinIO on [Quay.io](https://quay.io/). Port `9000` is used for connecting to the API and port `9001` is for accessing the Console in the browser.

Don’t forget to replace `username` and `password` in the environment variables with your authentication details. 

This is part of the output you get after initializing the container.

```
Status:         1 Online, 0 Offline.
S3-API: http://172.20.0.5:9000  http://127.0.0.1:9000
Console: http://172.20.0.5:9001 http://127.0.0.1:9001
```

`http://172.20.0.5:9000` is the URL required to configure PBM for using MinIO.

Once MinIO is running, you can access the console by going to `https://localhost:9001` in your browser.

![MinIO UI](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/minio-ui.png)

Now it’s time to create a bucket for storing your backups. Go to Bucket, click on Create Bucket and give a name (`mongo-backup`) to your bucket.

![Create Bucket](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/create-bucket.png?ref_type=heads)

Enable the following options if you need to:

* **Versioning** allows keeping multiple versions of the same object under the same key.
* **Object Locking** prevents objects from being deleted. Required to support retention and legal hold. It can only be enabled at bucket creation.
* **Quota** limits the amount of data in the bucket.

Then, click on the Create Bucket button.

Now, go to Identity → Users and click on Create User.

![Create User](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/create-user.png?ref_type=heads)

Write *User Name* (`pbm-user`) and *Password* and assign the required policies, at least `readwrite` policy. Click on Save.

The user has been created. Click on the user to see the details and create an *Access Key*.

![User Details](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/user-details.png?ref_type=heads)

Go to the Service Accounts section and click on Create Access Key.

![Service Accounts](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/service-accounts.png?ref_type=heads)

Access Key and Secret Key will be automatically generated, just click on Create.

![Access Key](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/access-key-created.png?ref_type=heads)

Download the access key as you will need those values for authenticating from PBM.

![Download Access Key](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/minio-user-access-key.png?ref_type=heads)

## Conclusion
Through this blog post, you learned how to set up MinIO to store the backup files generated by Percona Backup for MongoDB.