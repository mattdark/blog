From a previous [blog post], if you want to deploy a MongoDB cluster in a local development environment, you have to follow the next steps:

1. Create a Docker network

```
$ docker network create mongodbCluster
```

2. Create a primary node and add it to the network created before

```
$ docker run -d -p 27017:27017 --name mongo1 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo1
```

3. Once the primary node is created, the secondary nodes of the replica set must be created

```
$ docker run -d -p 27018:27017 --name mongo2 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo2
```

```
$ docker run -d -p 27019:27017 --name mongo3 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo3
```

4. And finally, initiate the replica set

```
$ docker exec -it mongo1 mongosh --eval "rs.initiate({
 _id: \"myReplicaSet\",
 members: [
   {_id: 0, host: \"mongo1\"},
   {_id: 1, host: \"mongo2\"},
   {_id: 2, host: \"mongo3\"}
 ]
})"
```

You can also use [Docker Compose](https://docs.docker.com/compose/) to deploy the cluster, without having to write all the previous commands.

Through this blog post, you will learn how to use Docker Compose for deploying a MongoDB cluster with replication enabled.

## Docker Compose
First, create a project directory:

```
$ mkdir mongodb-cluster
```

Then, create a `compose.yaml` file with the following content:

```
services:
  mongo1:
    image: mongo:6
    hostname: mongo1
    container_name: mongo1
    ports:
      - 27017:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo1"]
  mongo2:
    image: mongo:6
    hostname: mongo2
    container_name: mongo2
    ports:
      - 27018:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo2"]
  mongo3:
    image: mongo:6
    hostname: mongo3
    container_name: mongo3
    ports:
      - 27019:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo3"]
  mongosetup:
    image: mongo:6
    depends_on:
      - mongo1
      - mongo2
      - mongo3
    volumes:
      - .:/scripts
    restart: "no"
    entrypoint: [ "bash", "/scripts/mongo_setup.sh"]
```

1. Three services are deployed, `mongo1`, `monogo2`, `mongo3`, and `mongosetup`
2. The `mongo:6` is used as base to create the containers
3. Hostnames are assigned to the containers that will be part of the cluster, `mongo1`, `monogo2`, and `mongo3`
4. Names are assigned to the containers, `mongo1`, `monogo2`, and `mongo3`
5. Ports in the host machine are set, and they will redirect the requests to the ports in the containers
6. The `entrypoint` option is used to specify the command that will be executed once the container is initialized
7. The `mongosetup` service will create a container that is used to initiate the replica set

Once the `compose.yaml` file has been created, create a `scripts` directory, and a BASH script (`mongo_setup.sh`), inside that directory, with the following content:

```
$ mkdir scripts
$ touch scripts/mongo_setup.sh
```

```
#!/bin/bash
sleep 10

mongosh --host mongo1:27017 <<EOF
  var cfg = {
    "_id": "myReplicaSet",
    "version": 1,
    "members": [
      {
        "_id": 0,
        "host": "mongo1:27017",
        "priority": 2
      },
      {
        "_id": 1,
        "host": "mongo2:27017",
        "priority": 0
      },
      {
        "_id": 2,
        "host": "mongo3:27017",
        "priority": 0
      }
    ]
  };
  rs.initiate(cfg);
EOF
```

The above script will initiate the replica set.

Now run the following command to deploy the cluster:

```
$ docker compose up --wait
```

Your MongoDB cluster has been deployed, and it's ready to be used. If for any reason you need to remove the cluster, just run:

```
$ docker compose down
```

## Conclusion
Through this blog post, you learned how to deploy a MongoDB cluster with replication enabled, using Docker Compose.