import tweepy
import sys
import yaml

consumer_key = sys.argv[3]
consumer_secret = sys.argv[4]
access_token = sys.argv[1]
access_token_secret = sys.argv[2]

with open('articles/articles.yml') as f:
    # use safe_load instead load
    dict = yaml.safe_load(f)

title = dict['recent_articles'][0]['title']
url = sys.argv[5]

client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)

message = "New Blog Post: " + title + "\n" + url + "\n #DEVCommunity"

client.create_tweet(text=message)