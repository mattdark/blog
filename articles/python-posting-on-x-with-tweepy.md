There are a few libraries for Python that can be used to get access to the X API. I used [twitter](https://pypi.org/project/twitter/) previously for a project that is documented [here](dev.to/mattdark/automate-your-posting-process-on-dev-with-python-bash-and-gitlab-ci-5fm6) and can be found in this GitLab [repository](https://gitlab.com/mattdark/dev-blog-posting/). 

Due to the recent changes in the X API, and the library not being updated in about a year, I was getting the following error:

```bash
details: {'errors': [{'message': 'You currently have access to a subset of Twitter API v2 endpoints and limited v1.1 endpoints (e.g. media post, oauth) only. If you need access to this endpoint, you may need a different access level. You can learn more here: https://developer.twitter.com/en/portal/product', 'code': 453}]}
```

The solution was to used [Tweepy](https://github.com/tweepy/tweepy), a Python library well documented and prepared to be used with the version 2 of the X API.

Through this blog post, you will learn how to use Tweepy for posting updates on X.

## Create an app in the X Developer Portal
If you don't have a developer account, you must create one, in order to use the X API. You have to [apply](https://developer.twitter.com/en/apply-for-access) for getting access.

Once you're developer account is approved, go to [developer.twitter.com/en/portal/dashboard](https://developer.twitter.com/en/portal/dashboard) and [create a new app](https://developer.twitter.com/en/portal/apps/new), then generate the API Key and Secret, and an Access Token and Secret for your app.

## Installation
To install the latest version of Tweepy you can use pip:

```bash
pip install tweepy
```

## Posting from Python
Let's create a script to post updates on X.

First, import the Tweepy library into your project:

```python
import tweepy
```

Then, add the following variables:

```python
consumer_key = '' #API Key
consumer_secret = '' #API Key Secret
access_token = ''
access_token_secret = ''
```

Authenticate with your credentials to be able to use the X API:

```python
client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)
```

And finally, create a new post on X:

```python
text = 'Hello, World!'
client.create_tweet(text=text)
```

This is a basic example of Tweepy. Check the [documentation](https://docs.tweepy.org/en/stable/) for additional information and [examples](https://docs.tweepy.org/en/stable/examples.html).