The normal process for authenticating on your GitLab account would be to provide username and password. If two-factor authentication is enabled, an additional code would be requested. In that case, if you're a command line user, you must create an authentication token that would be used as a password. Another way to authenticate on your account is through SSH keys. 

In this blog post, you will learn how to configure your account and use SSH keys for authenticating and signing commits. I'm using [GitLab.com](https://gitlab.com), but it should work with your own instance.

## Create SSH keys
From the GitLab [documentation](https://docs.gitlab.com/ee/user/ssh.html), [ED25519](https://ed25519.cr.yp.to/) keys are recommended, as they are more secure and performant than RSA keys, according to the book [Practical Cryptography With Go](https://leanpub.com/gocrypto/read#leanpub-auto-chapter-5-digital-signatures).

To generate the key, run the following command:

```bash
ssh-keygen -t ed25519 -C "<comment>"
```

The comment is optional, but you may want to write your email address to identify what this key is for.

Then, it will ask you to set the path and filename for the key. Just press `Enter` and accept the default configuration.

```bash
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```

And finally, specify a passphrase:

```bash
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

For additional information, check the [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html) section in the GitLab documentation.

## Add the Public SSH Key to Your Account
Both a public and private key are generated, and you must add the public SSH key to your account.

Copy the content of the public key:

```bash
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

Then, add the key to your account:

1. Sign in to your account
2. Click on your profile picture
3. Go to Preferences
4. Then, open the SSH Keys section
5. Click on Add new key
6. In the Key box, paste the content of the key
7. In the title box, type a description
8. Select `Authentication & Signing` as usage type
9. Change expiration date if needed
10. Click on Add key

For additional information, go to the [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html) section in the GitLab documentation.

## Validate the Authentication
To check if you can authenticate to your account, run the following command:

```
ssh -T git@gitlab.example.com
```

Replacing `gitlab.example.com` with the URL of your GitLab instance. It will ask you to confirm that you want to add your GitLab instance as a known host, and typing the passphrase if configured.

## Configure Git to Sign Commits With Your Key
Once the key is added to your account, configure Git to use SSH for commit signing:

```bash
git config --global gpg.format ssh
```

And finally, specify which key to use:

```bash
git config --global user.signingkey ~/.ssh/id_ed25519
```

Replacing `~/.ssh/id_ed25519` with the path of your key.

Omit the `--global` option, if commit signing is not required for all the repositories in your local environment.

## Sign Your Commit
Once Git is configured, you can sign your commit by adding the `-S` option to the `git commit` command.

```bash
git commit -S -m "Commit description"
```

You can sign your commits automatically by configuring Git:

```bash
git config --global commit.gpgsign true
```

Now you can authenticate and sign your commits using SSH keys. Check the [documentation](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html) for additional information.