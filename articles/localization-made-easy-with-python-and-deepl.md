Today, I was working on a project and needed to find a way to localize some JSON files. I speak English as my second language and have some previous experience participating in localization projects, so there wouldn't have been any problem on localizing those files from Spanish to English, but how do you optimize the process when there are many strings to translate? Use the DeepL API and focus on validate that translations are correct.

## DeepL
Before using [DeepL API](https://www.deepl.com/en/pro-api?cta=menu-pro-api), you must create a free account.

* Go to the [Sign up page](https://www.deepl.com/en/signup?cta=checkout)
* Type your email and password
* Complete the CAPTCHA
* Fill the form
* Provide a valid credit card to verify you identity (Your credit card won't be charged unless you manually upgrade to DeepL API Pro)
* Accept the Terms and Conditions
* Click on Sign up for free

Once your account has been created, go to the [API Keys](https://www.deepl.com/en/your-account/keys) section, and copy your API Key or generate a new one.

## Python
For localizing the JSON files of your project, install [json-translate](https://github.com/Saigesp/json-translate). This library supports AWS Translate and DeepL.

```
pip install json-translate
```

Then, you must create an `.env` file in the root directory of your project, with the following content:

```
DEEPL_AUTH_KEY=YOUR_API_KEY
```

Replace `YOUR_API_KEY` with the value you copied previously from your DeepL account.

Another option for configuring this environment variable is by running the following command:

```
export DEEPL_AUTH_KEY=YOUR_API_KEY
```

Replace `YOUR_API_KEY` with the value you copied previously from your DeepL account.

## Localize your project
You have a JSON with the following content in Spanish:

```
{
  "tipo-perfil": {
    "label": "Tipo de perfil",
    "description": "Tipo de perfil",
    "tooltip": "Tipo de perfil",
    "validations": {
        "required": "El campo Tipo de perfil es requerido",
        "minMessage": "El número de caracteres debe ser de al menos {min}",
        "maxMessage": "El número de caracteres debe ser máximo de {max}",
        "regexMessage": "Formato de Tipo de perfil inválido"
    }
  }
}
```

In order to translate to English the values of every key in the JSON file, you must run the following command:

```
json_translate deepl perfil.json EN
```

Previous command will generate a `en.json` file with the following content:

```
{
  "tipo-perfil": {
    "label": "Profile type",
    "description": "Profile type",
    "tooltip": "Profile type",
    "validations": {
      "required": "The Profile type field is required",
      "minMessage": "The number of characters must be at least {min}",
      "maxMessage": "The number of characters must be a maximum of {max}",
      "regexMessage": "Invalid Profile Type Format"
    }
  }
}
```

You can change the name of the output file and the path by running the following command:

```
json_translate deepl perfil.json -o en/perfil.json EN
```

The localization is ready, but translations are not perfect with this kind of tools. You must review the result.

### Localize multiple files
I needed to localize multiple JSON files and created a BASH script to perform this task.

```
directory_path="es"
find "$directory_path" -type f | while IFS= read -r file; do
  b=$(basename $file)
  json_translate deepl "$file" EN -o "en/${b}"
done
```

The above code block does the following:

* Obtain the filenames in the `es` directory
* Translate the content with the `json_translate` command
* Save the output files in the `en` subdirectory with the same name

After running the above command you will get the content of the JSON files translated to English. And this is how you optimize the localization process.
