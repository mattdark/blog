This blog post is the third part of the series on Percona Backup for MongoDB (PBM) that includes the following articles:

1. Deploy a MongoDB Cluster With Docker
2. Object Storage with MinIO
3. Running Percona Backup for MongoDB With Docker

This blog post will teach you how to configure PBM to back up your databases using MinIO as the storage solution. As in the previous blog post, we will use Docker for configuring PBM in a development environment.

From [the documentation](https://docs.percona.com/percona-backup-mongodb/installation.html), you can install PBM in any of the following ways:
* Install from Percona repositories using the package manager of your operating system
* Run in Docker
* Deploy in Kubernetes
* Build from source
* Manual download

## Running PBM in Docker
### Create the PBM User

Follow the instructions from [the documentation](https://docs.percona.com/percona-backup-mongodb/install/configure-authentication.html) and create a user for PBM.

Log in to your MongoDB primary node

```
$ docker exec -it mongo1 mongosh
```

Create the role that allows any action on any resource.

```
db.getSiblingDB("admin").createRole({ "role": "pbmAnyAction",
      "privileges": [
         { "resource": { "anyResource": true },
           "actions": [ "anyAction" ]
         }
      ],
      "roles": []
   });
```

Create the user and assign the role you created to it.

```
db.getSiblingDB("admin").createUser({user: "pbmuser",
       "pwd": "secretpwd",
       "roles" : [
          { "db" : "admin", "role" : "readWrite", "collection": "" },
          { "db" : "admin", "role" : "backup" },
          { "db" : "admin", "role" : "clusterMonitor" },
          { "db" : "admin", "role" : "restore" },
          { "db" : "admin", "role" : "pbmAnyAction" }
       ]
    });
```

### Start the Container
Start the PBM container as follows:

```
$ docker run --name pbm --network mongodbCluster -e PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:<PORT>" -d percona/percona-backup-mongodb:latest
```

Where:

* `pbm` is the name you want to assign to your container,
* This container is added to the network previously created, `mongodbCluster`.
* `PBM_MONGODB-URI` is a MongoDB Connection URI string used to connect to the primary node of your cluster
* The `<HOST>` will be the hostname (`mongo1`) of your primary node, the `<PORT>` will be `27017`.

### Set Up PBM
Create a `pbm_config.yaml` file with the following content:

```
storage:
  type: s3
  s3:
    endpointUrl: "http://<HOST>:9000"
    region: us-east-1
    bucket: <BUCKET_NAME>
    prefix: data/pbm/test
    credentials:
      access-key-id: accessKey
      secret-access-key: secretKey
```

Where:

* <HOST> is the hostname of the container running PBM.
* `<BUCKET_NAME>` is the name (`mongo-backup`) of the bucket you created while configuring MinIO.
* The values of `acces-key-id` and `secret-access-key` can be obtained from the `credentials.json` file downloaded after creating an access key for the MinIO user. 

Then, copy that file to your container:

```
$ docker cp pbm_config.yaml container_name:/tmp/pbm_config.yaml
```

`container_name` is the name (`pbm`) of the PBM container.

Log in to your container:

```
$ docker exec -it pbm bash
```

And run:

```
$ pbm config --file /tmp/pbm_config.yaml
```

### Set Up `pbm-agent`
Before doing backups, you must start `pbm-agent` on every server with the `mongod` node installed.
For doing this, you must log in to MongoDB, install PBM and run `pbm-agent`:

```
$ docker exec -it <NODE> bash
```

Replace NODE with mongo1, mongo2, and mongo3. You need to log into every node in your cluster and run the following commands.

Install all the required packages:

```
$ apt update && apt install curl cron nano && curl -O https://repo.percona.com/apt/percona-release_latest.generic_all.deb && apt install -y gnupg2 lsb-release ./percona-release_latest.generic_all.deb && apt update && percona-release enable pbm release && apt update && apt install -y percona-backup-mongodb
```

The above command will install PBM.

And finally, set the PBM_MONGODB_URI and run pbm-agent.

```
PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:27017/?authSource=admin" && export PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:27017/?authSource=admin&replSetName=myReplicaSet" && pbm-agent
```

Where `<HOST>` is the hostname (`mongo1`) of your primary node.

Once PBM is running on every node of your cluster, you can do backups.

### Start a Backup
You can do a complete backup of the databases and collections in your MongoDB Server by running the following command from the `pbm` container:

```
$ docker exec -it pbm bash
```

```
pbm backup --type=TYPE
```

You can specify what type of backup you wish to make: `physical` or `logical`.

When `physical` backup is selected, Percona Backup for MongoDB copies the contents of the `dbpath` directory (data and metadata files, indexes, journal, and logs) from every shard and config server replica set to the backup storage.

During `logical` backups, Percona Backup for MongoDB copies the actual data to the backup storage. When no `--type` flag is passed, Percona Backup for MongoDB makes a logical backup.

To list the backups available in your storage, run the following command:

```
pbm list
```

You will get the following output:

```
Backup snapshots:
  2023-03-10T19:32:35Z <logical> [restore_to_time: 2023-03-10T19:32:40Z]
```

For more information, check [the documentation](https://docs.percona.com/percona-backup-mongodb/usage/start-backup.html).

#### Selective Backup / Restore
When you run `pbm backup`, you get a complete backup of the databases in your server. But if you want to get a backup of a database or a collection, you may want to try selective backup and selective restore.

For example, if you have some databases in your server, and you need to back up a database or a collection in your database, you can do a selective backup by running any of the following commands:

```
pbm backup --ns=staff.Payments
```

```
pbm backup --ns=Invoices.*
```

With the first command, you will get a backup of the `Payments` collection in the `staff` database.

With the second command, you will get a backup of the `Invoices` database and all the collections it includes.

If you run `pbm list`, you will get the following output:

```
Backup snapshots:
  2023-03-10T19:49:42Z <logical, selective> [restore_to_time: 2023-03-10T19:49:46Z]
```

To restore a specific database or a collection, run the `pbm restore` command:

```
pbm restore <backup_name> --ns <database.collection>
```

Where `<backup_name>` is the name of your backup (i.e. `2023-03-10T19:49:42Z`) and `<database.collection>` is the name of the database and collection you want to restore.

For more information about selective backup and selective restore, check this page in [the documentation](https://docs.percona.com/percona-backup-mongodb/usage/start-backup.html).

## Conclusion
After configuring a three-node MongoDB cluster with replication enabled, MinIO and Percona Backup for MongoDB, you're ready to try this tools in your local development environment.