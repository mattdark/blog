When configuring a new GitLab account on [GitLab.com](https://gitlab.com/) you would get access to a 30-day GitLab Ultimate trial and would be required to validate your account with a credit card for using free minutes of [GitLab Runner](https://docs.gitlab.com/runner/).

For those who are privacy concerned, you can [install](https://docs.gitlab.com/runner/install/) GitLab Runner on a infrastructure you own or manage. It can be installed:

- In a container, using [Docker](https://docs.gitlab.com/runner/install/docker.html), [Kubernetes](https://docs.gitlab.com/runner/install/kubernetes.html) or [OpenShift](https://docs.gitlab.com/runner/install/openshift.html).
- By downloading a binary manually and installing it on [GNU/Linux](https://docs.gitlab.com/runner/install/linux-manually.html), [macOS](https://docs.gitlab.com/runner/install/osx.html), [Windows](https://docs.gitlab.com/runner/install/windows.html) or [FreeBSD](https://docs.gitlab.com/runner/install/freebsd.html).
- By using a [repository](https://docs.gitlab.com/runner/install/linux-repository.html) for rmp/deb packages.

## Run GitLab Runner in a container
Before installing GitLab Runner, make sure Docker is up and running on your system. On GNU/Linux follow the instructions in the [documentation](https://docs.docker.com/engine/install/) or go directly to the section that covers the instructions for your distro:

- [Centos](https://docs.docker.com/engine/install/centos)
- [Debian](https://docs.docker.com/engine/install/debian)
- [Fedora](https://docs.docker.com/engine/install/fedora)
- [RHEL](https://docs.docker.com/engine/install/rhel/)
- [SLES](https://docs.docker.com/engine/install/sles/)
- From [binaries](https://docs.docker.com/engine/install/binaries/)

Don't forget to follow [post-installation](https://docs.docker.com/engine/install/linux-postinstall/) steps.

### Install the Docker image and start the container

For installing the Docker image, create a Docker volume:

```
docker volume create gitlab-runner-config
```

Start the GitLab Runner container:

```
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

### Register your runner

Before using your recently installed GitLab Runner, you have to [register](https://docs.gitlab.com/runner/register/index.html#docker) it.

Go to the settings of a GitLab project that you want to use GitLab Runner for and obtain the registration token. In your GitLab repository, go to **Settings** --> **CI/CD** and expand the *Runners* section. From the *Specific runners* section, copy the registration token. Don't forget to disable *Shared runners*.

Now from the terminal, run the following command:

```
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

It will ask you for the following information:

- Enter the GitLab instance URL: https://gitlab.com/
- Enter the registration token: The one you copied before
- Enter a description for the runner: What you're planning to use it for
- Enter tags for the runner: Tags are comma separated and assign manually to jobs in your CI/CD pipelines. You can edit them later
- Enter optional maintenance note for the runner.
- Enter an executor: docker
- Enter the default Docker image: ruby:2.7. Default image to use when not defined in your *.gitlab-ci.yml* file.

If you don't assign tags to the jobs in your CI/CD pipeline, the GitLab Runner won't start as it is configured by default to pick jobs that have any of the tags specified in the configuration.

For changing this behavior and let the GitLab Runner pick jobs without tags, expand the *Runners* section from **Settings** --> **CI/CD** and click the edit button in the runner you want to modify from the **Available specific runners** section. Then check the following option: *Indicates whether this runner can pick jobs without tags*.

By default runners are configured to use them only with a specific project, If you want other projects in your account to use this runner, uncheck the option: *When a runner is locked, it cannot be assigned to other projects*. In other projects, expand the *Runners* section from **Settings** --> **CI/CD**, go to the *Available specific runners* and click on *Enable for this project* to choose the runner you want use from the available ones.

Now you're ready to use GitLab CI and configure your CI/CD pipelines using your own GitLab Runner.